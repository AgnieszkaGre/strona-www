 function clock() {
            
    var today = new Date();

    var day = today.getDate();
    var month = today.getMonth();
    var monthName = ["stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia", "września", "października", "listopada", "grudnia"] 
    
    var year = today.getFullYear();
    var hour = today.getHours();
        if (hour<10) hour = "0" + hour;
    var minute = today.getMinutes();
        if (minute<10) minute = "0" + minute;
    var second = today.getSeconds();
        if (second<10) second = "0" + second;
            
    document.getElementById("clock").innerHTML = day+" "+monthName[month]+" "+year+"  "+hour+":"+minute+":"+second;
            
    setTimeout("clock()",1000);
}   
